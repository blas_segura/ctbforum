<%@ Page Language="C#" MasterPageFile="AspNetForumMaster.Master" AutoEventWireup="true"
    CodeBehind="faq.aspx.cs" Inherits="aspnetforum.faq" Title="FAQ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHEAD" runat="server">
    <style type="text/css">
        div.column1 {
            width: 49%;
            float: left;
        }

        div.column2 {
            width: 50%;
            float: right;
        }
        .homeLink {
        padding-top:20px;
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AspNetForumContentPlaceHolder" runat="server">
    <div class="homeLink">
            <a href="default.aspx">Home</a>
        </div>
    <div class="faqbody">
    <h1>Forum Frequently Asked Questions</h1>
    <br />
    <br />
    <div id="faqlinks">
        <div class="column1">
           <dl>
                <dt><strong>Login and Registration Issues</strong></dt>
                <dd><a href="#f00" name="f00">How can I recover my lost/forgotten password?</a></dd>
            </dl>
            <dl>
                <dt><strong>User Preferences and settings</strong></dt>
                <dd><a href="#f10" name="f10">How do I change
                    my settings?</a></dd>
                <dd><a href="#f14" name="f14">How do I show an image next to my username?</a></dd>
            </dl>
            <dl>
                <dt><strong>Topic Subscriptions</strong></dt>
                <dd><a href="#f81" name="f81">How do
                    I subscribe to specific forums or topics?</a></dd>
                <dd><a href="#f82" name="f82">How do I remove
                        my subscriptions?</a></dd>
            </dl>
            <dl>
                <dt><strong>Posting Issues</strong></dt>
                <dd><a href="#f20" name="f20">How do I post a topic in
                    a forum?</a></dd>
                <dd><a href="#f21" name="f21">How do I edit or delete a post?</a></dd>
                
                <dd><a href="#f27" name="f27">Why can't I add attachments?</a></dd>
                <dd><a
                    href="#f211" name="f211">Why does my post need to be approved?</a></dd>
            </dl>

        </div>
        <div class="column2">
            
            <dl>
                <dt><strong>Accessing a forum</strong></dt>
                <dd><a href="#f26" name="f26">Why can't I access a forum?</a></dd>
                <dd><a href="#f28" name="f28">How do I navigate back to my primary tabs?</a></dd>
                <dd><a href="#f29" name="f29">Why is there more than one forum?</a></dd>
            </dl>
            <dl>
                <dt><strong>Formatting and Topic Types</strong></dt>
                <dd><a href="#f31" name="f31">Can I use HTML?</a></dd>
                <dd><a
                    href="#f32" name="f32">What are Smilies?</a></dd>
                <dd><a href="#f33" name="f33">Can I post images?</a></dd>
                <dd><a
                    href="#f36" name="f36">What are sticky topics?</a></dd>
                <dd><a href="#f37" name="f37">What are closed topics?</a></dd>
            </dl>

            <dl>
                <dt><strong>Attachments</strong></dt>
                <dd><a href="#f90" name="f90">What is maximum attachment size
                    allowed on this forum?</a></dd>
            </dl>
        </div>
    </div>
    <div style="clear: both">

 <hr />
        <h2>Login and Registration Issues</h2>
        <dl>
            <dt id="f00"><strong>How can I recover my lost/forgotten password?</strong></dt>
            <dd>If a user has lost the password it must recovered using the website and not the forum.
                We have hid the lost my password button from the forum.After click in reset password 
                from the website, they will receive an email with a link to reset the password.</dd>
            <dd><a
                href="#faqlinks">Top</a></dd>
        </dl>

        <hr />
        <h2>User Preferences and settings</h2>
        <dl>
            <dt id="f10"><strong>How do I change my settings?</strong></dt>
            <dd>If you are a registered
                user, all your settings are stored in the forum database. To alter them, click your
                username in the upper right corner while logged in.</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <dl>
            <dt id="f14"><strong>How do I show an image next to my username?</strong></dt>
            <dd>If you experience problems and unable to use avatars, please contact a forum administrator
                for support - <a href="mailto:info@changetoolbox.com">info@changentoolbox.com</a></dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <hr />
        <h2>Posting Issues</h2>
        <dl>
            <dt id="f20"><strong>How do I post a topic in a forum?</strong></dt>
            <dd>To post a new
                topic in a forum, click the relevant button on either the forum or topic screens.
             </dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <dl>
            <dt id="f21"><strong>How do I edit or delete a post?</strong></dt>
            <dd>Unless you are
                a forum administrator or moderator, you can only edit or delete your own posts.
                You can edit a post by clicking the "edit" button for the relevant post.</dd>
            <dd><a
                href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>
        <dl>
            <dt id="f27"><strong>Why can't I add attachments?</strong></dt>
            <dd>The forum administrator
                may not have allowed attachments. Contact the forum administrator if you are unsure
                about why you are unable to add attachments.<a href="mailto:info@changetoolbox.com"> info@changetoolbox.com</a>
            </dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <dl>
            <dt id="f211"><strong>Why does my post need to be approved?</strong></dt>
            <dd>The forum
                administrator may have decided that posts in the forum you are posting to require
                review before submission.</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>
        <h2>Accessing Forum</h2>
        <dl>
            <dt id="f26"><strong>Why can't I access a forum?</strong></dt>
            <dd>Some forums may be limited to certain users or groups. To view, read, post or perform
                another action you may need special permissions. Contact a forum administrator.
                <a href="mailto:info@changetoolbox.com"> info@changetoolbox.com</a>
            </dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>
        <dl>
            <dt id="f28"><strong>How do I navigate back to my primary tabs?</strong></dt>
            <dd>How do I navigate back to my primary tabs?
                Each question enables you to 'click' on the word 'top' and this will take you to the
                top of the page and by scrolling up you will see the main navigation tabs.</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>
         <dl>
            <dt id="f29"><strong>Why is there more than one forum? </strong></dt>
            <dd>Those are the threads, it is to filter the search by the selected thread</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>
        <hr />
        <h2>Formatting and Topic Types</h2>
        <dl>
            <dt id="f31"><strong>Can I use HTML?</strong></dt>
            <dd>No. It is not possible to post
                HTML on this forum and have it rendered as HTML. Most formatting which can be carried
                out using HTML can be applied using the formatting toolbar when posting a message.</dd>
            <dd><a
                href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <dl>
            <dt id="f32"><strong>What are Smilies?</strong></dt>
            <dd>Smilies, or Emoticons, are small
                images which can be used to express a feeling using a short code, e.g. :) denotes
                happy, while :( denotes sad. The full list of emoticons can be seen in the posting
                form. Try not to overuse smilies, however, as they can quickly render a post unreadable
                and a moderator may edit them out or remove the post altogether. The forum administrator
                may also have set a limit to the number of smilies you may use within a post.</dd>
            <dd><a
                href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <dl>
            <dt id="f33"><strong>Can I post images?</strong></dt>
            <dd>Yes, images can be shown in your posts, providing the permissions are available. 
                You may need to contact the administrator if you incur any problems, insert link as above. 
                You cannot link to pictures stored on your own Pc, nor images stored behind authentication
                mechanisms, e.g. hotmail, yahoo mail boxes, password protected sites, etc.
                To display the image use the appropriate button on the formatting toolbar 
                which adds an [img] tag. </dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <dl>
            <dt id="f36"><strong>What are sticky topics?</strong></dt>
            <dd>Sticky topics within the
                forum appear on top of other topics in a forum. They are often quite important so
                you should read them whenever possible. Topics are made sticky by Administrators
                and Moderators.</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <dl>
            <dt id="f37"><strong>What are closed topics?</strong></dt>
            <dd>Topics may be closed for many reasons and are where the user can no longer reply. </dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <hr />
        <h2>User Levels and Groups</h2>
        <dl>
            <dt id="f40"><strong>What are Administrators?</strong></dt>
            <dd>Administrators are members
                assigned with the highest level of control over the entire forum. These members
                can control all facets of forum operation, including setting permissions, banning
                users, creating usergroups or moderators, etc., dependent upon the forum founder
                and what permissions he or she has given the other administrators. They may also
                have full moderator capabilities in all forums.</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <dl>
            <dt id="f41"><strong>What are Moderators?</strong></dt>
            <dd>Moderators are individuals
                (or groups of individuals) who look after the forums from day to day. They have
                the authority to edit or delete posts and lock, unlock, move, delete topics in the
                forum they moderate. Generally, moderators are present to prevent users from going
                off-topic or posting abusive or offensive material.</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <dl>
            <dt id="f42"><strong>What are usergroups?</strong></dt>
            <dd>User Groups are created and handled by Administrators. 
                User groups are groups of users that divide the community into sections 
                and can belong to several groups. </dd>
            <dd><a
                href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <hr />
<%--        <h2>Private Messaging</h2>
        <dl>
            <dt id="f50"><strong>I cannot send private messages!</strong></dt>
            <dd>You are not registered
            and/or not logged on. Please register and log in to be able to send PMs.</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>
        <hr />
        <dl>
            <dt id="f52"><strong>I have received a spamming or abusive message from someone on this
                forum!</strong></dt>
            <dd>Please e-mail the forum administrator with a full copy of the
                    message you received. The forum administrator can then take action.</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <hr />
        <h2>Searching the Forums</h2>
        <dl>
            <dt id="f70"><strong>How can I search a forum or forums?</strong></dt>
            <dd>Click the
                "Search" link, enter a search term in the search box. Click the "search" button.</dd>
            <dd><a
                href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>
        <dl>
            <dt id="f74"><strong>How can I find my own or someone else's posts and topics?</strong></dt>
            <dd>Click
                a username in the topic you have answered to, and view your profile as other's see
                it.</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <hr />--%>
        <h2>Topic Subscriptions</h2>
        <dl>
            <dt id="f81"><strong>How do I subscribe to specific forums or topics?</strong></dt>
            <dd>To
                subscribe to a specific forum, click the "watch this forum for new topics" link
                upon entering the forum. To subscribe to a topic, reply to the topic with the subscribe
                checkbox checked or click the "watch this topic for replies" link within the topic
                itself.</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>
        <dl>
            <dt id="f82"><strong>How do I remove my subscriptions?</strong></dt>
            <dd>To remove your
                subscriptions, go to your profile (by clicking your username on the top) and click
                "My subscriptions".</dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <hr />
        <h2>Attachments</h2>
        <dl>
            <dt id="f90"><strong>What is maximum attachment size allowed on this forum?</strong></dt>
            <dd>If you are unsure what is it, contact the forum administrator for assistance.<a href="mailto:info@changetoolbox.com"> info@changetoolbox.com</a></dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>

        <hr />
        <h2>Ratings and reputations</h2>
        <dl>
            <dt id="f91"><strong>What are ratings?</strong></dt>
            <dd>User can rate messages up and down.
            This gives users "reputation" and makes finding valuable information easier.
            </dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>
        <dl>
            <dt id="f92"><strong>How do I rate posts?</strong></dt>
            <dd>To rate  a post
                simply click the thumbs-up or thumbs-down icon next to it. the icon is displayed to registered users only.
                You can vote only once.
            </dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>
        <dl>
            <dt id="f93"><strong>How do I filter unrated posts?</strong></dt>
            <dd>Each topic has a
                "hide unrate posts" link on top. You can hide unrated "noise" and read the valuable information only.
            </dd>
            <dd><a href="#faqlinks" name="faqlinks">Top</a></dd>
        </dl>
    </div>
        </div>
</asp:Content>
